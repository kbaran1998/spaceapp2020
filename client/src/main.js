import Vue from "vue";
import Vuex from "vuex";
import {
  BootstrapVue,
  IconsPlugin
} from "bootstrap-vue";
import VueRouter from "vue-router";
import RouterPrefetch from "vue-router-prefetch";
import App from "./App";
// TIP: change to import router from "./router/starterRouter"; to start with a clean layout
import router from "./router/index";
import BlackDashboard from "./plugins/blackDashboard";
import i18n from "./i18n";
import "./registerServiceWorker";
import store from "./store";

Vue.use(BlackDashboard);
Vue.use(VueRouter);
Vue.use(RouterPrefetch);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(Vuex);
/* eslint-disable no-new */
new Vue({
  router,
  i18n,
  store,
  render: (h) => h(App),
}).$mount("#app");
