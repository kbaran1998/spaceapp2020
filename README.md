# Space App

## Creators

- Krzysztof Baran
- Victor Wernet
- Mostafa Khattat

## How to run/setup this project

1. Run the server-side Flask app in one terminal window (venv is not required):

### Windows Powershell

```sh
cd server
python3.7 -m venv env (optional)
.\venv\Scripts\activate  (optional)
(env)$ pip install -r requirements.txt
(env)$ python app.py
```

### Linux

```sh
$ cd server
$ python3.7 -m venv env  (optional)
$ source env/bin/activate  (optional)
(env)$ pip install -r requirements.txt
(env)$ python app.py
```

Navigate to [http://localhost:5000](http://localhost:5000)

1. Run the client-side Vue app in a different terminal window:

### Windows Powershell and Linux

```sh
$ cd client
$ npm install
$ npm run serve
```

Navigate to [http://localhost:8080](http://localhost:8080)
